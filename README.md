# How to build

* Install asciidoctor and asciidoctor-diagram plugin
* Execute the following list of commands:

```bash
$ asciidoctor -r asciidoctor-diagram 001-diagrams.adoc

$ node asciidoctor-revealjs.js
```
